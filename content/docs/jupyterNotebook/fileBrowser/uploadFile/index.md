---
title: "ファイルをアップロードする"
weight: 30
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

1. `Upload`にアップロードしたいファイルをドラッグ&ドロップする  
   ![](images/NotebookFileBrowser.png)  
2. ドラッグ&ドロップしたファイル名の右にある`Upload`をクリックする
   ![](images/NotebookFileBrowserConfirm.png)
