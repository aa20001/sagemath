---
title: "名前を変更する"
weight: 26
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---
1. 名前を変更したいファイル又はフォルダの左にあるチェックボックスをクリックして選択する  
    ![](images/NotebookFileCheck.png)  
2. `Rename`をクリックする  
    ![](images/NotebookFileRename.png)  
3. 下線部に適切な名前を入力する 
    ファイル名を変更するときは拡張子を消さないように注意(拡張子とはファイル名の最後についている`.`以降の文字列のこと画像では`.ipynb`のこと)  
    ![](images/NotebookFileRenameDialog.png)  
4. `Rename`をクリックする  
    ![](images/NotebookFileRenameConfirm.png)
    