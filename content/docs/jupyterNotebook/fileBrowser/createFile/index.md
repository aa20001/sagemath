---
title: "ファイルを作成する"
weight: 25
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

1. `New`をクリックする  
    ![](images/NotebookNew.png)  
2. 作成したいファイル形式を選択する  
   `Notebook`の部分に書いてあるものは`.ipynb`形式のファイルが作成される  
   `Console`の部分に書いてあるものは初心者は使う必要がないので無視するように  
   `Other`の部分に書いてあるものはその他形式で作成されるという意味  
   `.ipynb`形式のファイルが複数あるのはカーネルの種類が複数あるからでカーネルの種類によってできることが変わる  
    SageMathを使用するときは`SageMath`から始まるカーネルを使用しなければならない  
    ![](images/NotebookNewMenu.png)