---
title: "フォルダを作成する"
weight: 10
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

1. `new`をクリックする  
    ![](images/NotebookNew.png)  
2. `Folder`をクリックする  
    ※ `Folder`が見えない場合は下にスクロールする  
    ![](images/NotebookNewMenu.png)  
3. できた`Untitled folder`の右のチェックボックスをクリックしチェックをつける  
    ![](images/NotebookCreatedFolder.png)  
4. `Rename`をクリックする  
    ![](images/NotebookFolderChecked.png)  
5. テキストボックス内の文字列を作成したいフォルダ名に変更する  
    ![](images/NotebookFolderRenameMenu.png)  
6. `Rename`をクリックしてフォルダ名の変更を反映する  
   ![](images/NotebookFolderRename.png)  
