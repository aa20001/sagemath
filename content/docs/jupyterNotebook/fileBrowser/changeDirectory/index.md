---
title: "表示しているフォルダ(作業ディレクトリ)を切り替える"
weight: 20
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

# フォルダ間の移動
## 初期ディレクトリへの移動
<img src="images/NotebookRootIcon.png" style="height: 1em">をクリックする  
![](images/NotebookRoot.png)

## 親ディレクトリへの移動
パスの2番目に右側のリンクをクリックする  
![](images/NotebookParentDirectory.png)  
または`..`をクリックする  
![](images/NotebookParentDirectory1.png)

## 子ディレクトリへの移動
移動したいフォルダをクリックする  
![](images/NotebookChildrenDirectory.png)
