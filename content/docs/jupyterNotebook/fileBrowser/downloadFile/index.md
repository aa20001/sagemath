---
title: "ファイルをダウンロードする"
weight: 100
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

## ファイルブラウザからダウンロードする
1. ダウンロードしたいファイルの左にあるチェックボックスをクリックして選択する  
    <img src="images/NotebookFileBrowser.png" />  
2. `Download`をクリックする  
   <img src="images/NotebookFileBrowserSelected.png" />

## 開いているファイルをダウンロードする
1. ダウンロードしたいファイルが表示されていることを確認する  
   もしされていないなら表示しておく
2. `File`をクリックする  
   <img src="images/NotebookFile.png" />
3. `Download as`にカーソルを合わせる
   <img src="images/NotebookFileMenu.png" />
4. `Notebook`をクリックする
   <img src="images/NotebookDownloadAs.png" />