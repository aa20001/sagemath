---
title: "表示しているフォルダ(作業ディレクトリ)を切り替える"
weight: 20
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

# ファイルブラウザの表示
もし以下のような画面になっているときは矢印の部分をクリックする  
<img src="images/LabFileBrowserHidden.png" />  

# フォルダ間の移動
## 初期ディレクトリへの移動
<img src="images/LabRootIcon.png" style="height: 1em">をクリックする
![](images/LabFileRoot.png)

## 親ディレクトリへの移動
パスの一番右側をクリックする
![](images/LabParentDirectory.png)

## 子ディレクトリへの移動
開きたいフォルダをダブルクリックする
![](images/LabChildrenDirectory.png)
