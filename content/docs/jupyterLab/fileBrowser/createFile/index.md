---
title: "ファイルを作成する"
weight: 25

# bookFlatSection: false

# bookToc: true

# bookHidden: false

# bookCollapseSection: false

# bookComments: false

# bookSearchExclude: false
---

1. もし以下のような画面になっているときは矢印の部分をクリックする  
   <img src="images/LabFileBrowserHidden.png" />
2. <img src="images/LabNewButton.png" style="height: 1em;">をクリックする  
   ![](images/LabNewMenu.png)
3. 作成したいファイル形式を選択する
   `Notebook`の部分に書いてあるものは`.ipynb`形式のファイルが作成される  
   `Console`の部分に書いてあるものは初心者は使う必要がないので無視するように  
   `Other`の部分に書いてあるものはその他形式で作成されるという意味  
   `.ipynb`形式のファイルが複数あるのはカーネルの種類が複数あるからでカーネルの種類によってできることが変わる  
   SageMathを使用するときは`SageMath`から始まるカーネルを使用しなければならない

   ![](images/LabNewMenuIpynb.png)
   ![](images/LabNewMenuConsole.png)  
   ![](images/LabNewMenuOther.png)
   