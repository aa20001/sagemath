---
title: "ファイルをアップロードする"
weight: 30
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

1. もし以下のような画面になっているときは矢印の部分をクリックする
    <img src="images/LabFileBrowserHidden.png" />
2. アップロードしたいファイルを矢印の部分にドラッグ&ドロップする
    <img src="images/LabMain.png" />