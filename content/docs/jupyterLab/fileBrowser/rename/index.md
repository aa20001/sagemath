---
title: "名前を変更する"
weight: 26
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

1. もし以下のような画面になっているときは矢印の部分をクリックする  
    <img src="images/LabFileBrowserHidden.png" />  
2. 名前を変更したいファイル又はフォルダをクリックして選択する  
    ![](images/LabFileSelect.png)  
3. 選択したファイル又はフォルダを右クリックする  
    ![](images/LabFileSelected.png)  
4. `Rename`をクリックする  
    ![](images/LabFileContextMenu.png)  
5. 適切な名前を入力し`Enter`を押す  
    ファイル名を変更するときは拡張子を消さないように注意(拡張子とはファイル名の最後についている`.`以降の文字列のこと画像では`.ipynb`のこと)
    ![](images/LabFileRename.png)  