---
title: "フォルダを作成する"
weight: 10
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

1. もし以下のような画面になっているときは矢印の部分をクリックする  
   <img src="images/LabFileBrowserHidden.png" />  
2. <img src="images/LabDownloadButton.png" style="height: 2em">をクリックしてフォルダを作成する  
   ![](images/LabFileBrowser.png)  
3. 適切なフォルダー名を入力し`Enter`キーを押す  
   ![](images/LabFolderRename.png)