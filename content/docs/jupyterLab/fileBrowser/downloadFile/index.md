---
title: "ファイルをダウンロードする"
weight: 100
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

## ファイルブラウザからダウンロードする
1. もし以下のような画面になっているときは矢印の部分をクリックする  
   <img src="images/LabFileBrowserHidden.png" />  
2. ダウンロードしたいファイルをクリックして選択する  
   <img src="images/LabFileBrowserSelected.png" />
3. 選択した場所を右クリックする
   <img src="images/LabFileBrowserContextMenu.png" />
4. `Download`をクリックする
   <img src="images/LabFileBrowserContextMenu.png" />

## 開いているファイルをダウンロードする
1. ダウンロードしたいファイルが表示されていることを確認する  
   もしされていないなら表示しておく  
2. `File`をクリックする  
   <img src="images/LabFile.png" />
3. `Download`をクリックする
   <img src="images/LabFileMenu.png" />