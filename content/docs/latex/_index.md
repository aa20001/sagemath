---
title: "Latex"
weight: 30
# bookFlatSection: false
# bookToc: true
# bookHidden: false
bookCollapseSection: true
# bookComments: false
# bookSearchExclude: false
---

# TeXとは  
> $\TeX$は、ドナルド・クヌース (Donald E. Knuth) が開発し、広く有志による拡張などが続けられている組版処理システムである。  
  [Wikipedia](https://ja.wikipedia.org/wiki/TeX#cite_ref-15)

> $\TeX$は1978年に Donald E. Knuth が発表した組版システムです  
  https://blog.wtsnjp.com/2016/12/19/tex-and-latex/

# LaTeXとは  
$\LaTeX$とは$\TeX$を手軽に使うためのマクロパッケージなど  
> [Wikipedia](https://ja.wikipedia.org/wiki/LaTeX)


# AMS-LaTeXとは  
> AMS-$\LaTeX$とはアメリカ数学会向けに開発された$\LaTeX$のドキュメントクラス。パッケージのコレクション。  
  [Wikipedia](https://ja.wikipedia.org/wiki/AMS-LaTeX)

JupyterNotebook上で数式を書くときはほぼこのパッケージを使用している。

# MathJaxやKaTeXについて  
MathJaxや$\KaTeX$とは$\LaTeX$形式で記述された数式などをWebブラウザで表示するためのライブラリです。  
どちらも$\LaTeX$をベースとしていますが拡張コマンドや解釈の厳密さ、使えるコマンドに違いがあります。  
このサイトでは$\KaTeX$を使って数式などを表示しています。  

