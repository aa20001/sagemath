---
title: "よく使うLaTeXマクロ・コマンド"
weight: 20
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---
ここではよく使うマクロやコマンドについて書いてあります。  
必要であれば[KaTeX公式サイト](https://katex.org/docs/supported.html)あたりを見ると探したいものが見つかるかもしれません。  


# 基礎編  
### 分数  
分数には2種類あり数式モードに合わせる`\frac`と数式モード問わずディスプレイスタイルで表示する`\dfrac`がある。  
* $\frac{1}{2}$  
  `\frac{1}{2}`
* $\dfrac{1}{2}$  
  `\dfrac{1}{2}`

### 下付き文字(添字)  
* $a_n$  
  `a_n`
### 上付き文字(冪乗)
* $a^n$  
  `a^n`

### 平方根・立方根  
* $\sqrt{x}$  
  `\sqrt{x}`  

* $\sqrt[3]{x}$  
  `\sqrt[3]{x}`

### 括弧等の大きさを括弧の中身に合わせる  
* $\left(\dfrac{1}{2}\right)$  
  `\left(\dfrac{1}{2}\right)`  

また、括弧等の大きさを括弧の中身に合わせた上で片方を表示したくないときは次のようにする。  
* $\left(\dfrac{1}{2}\right.$  
  `\left(\dfrac{1}{2}\right.`  
  
### 特殊文字のエスケープ  
$\LaTeX$には$\LaTeX$が特別扱いする文字が存在している。  
そのような文字を表示したい場合はその文字の前に`\`を置く。  

* {{<raw>}}$ \# ${{</raw>}}  
  `\#`

* {{<raw>}}$ \{\} ${{</raw>}}  
  `\{\}`

### ✖︎記号(外積)  
* $A\times B$
  `A\times B` 

### ±とその逆  
* $\pm$  
  `\pm`  
* $\mp$  
  `\mp`

### ドットたち(ドット積等)  
* $A\cdot B$  
  `A\cdot B` 

* $A_1, A_2,\dots, A_n$  
  `A_1, A_2\dots A_n`  

* $A_1+A_2+\dots+A_n$  
  `A_1+A_2+\dots+A_n`  

他にもいろいろあるが省略  
(https://mathlandscape.com/latex-dots/)  


### ベクトル  
* {{<raw>}}$\boldsymbol{u}${{</raw>}}  
  `\boldsymbol{u}`  
  
* {{<raw>}}$\vec{u}${{</raw>}}  
  `\vec{u}`  

* {{<raw>}}$\overrightarrow{u}${{</raw>}}  
  `\overrightarrow{u}`  

### 連立方程式  
* {{<raw>}}$\left\{\begin{alignedat}{2}10&x+&3&y=2\\3&x+&13&y=4\end{alignedat}\right.${{</raw>}}  
  `\left\{\begin{alignedat}{2}10&x+&3&y=2\\3&x+&13&y=4\end{alignedat}\right.`  

### 条件分岐された関数(区分線形関数、区分多項式)  
* {{<raw>}}$f(x)=\begin{cases}a(x-\alpha)&\operatorname{if}\ 0< x\\ b(x-\beta)&\operatorname{if}\ x< 0 \end{cases}${{</raw>}}  
  `f(x)=\begin{cases}a(x-\alpha)&\operatorname{if}\ 0< x\\ b(x-\beta)&\operatorname{if}\ x< 0 \end{cases}`

### デフォルトで定義されていない演算子・関数を使う  
* $\operatorname{Sin}^{-1}(1)$  
  `\operatorname{Sin}^{-1}(1)`  

### 数式中で漢字などのテキストを書く  
数式中に漢字などのテキストを書くときは`\text`を使います。  
* $\text{hello}$  
  `\text{hello}`

### 行列・行列式  
* {{<raw>}}$\begin{pmatrix}1&2&3\\4&5&6\\7&8&9\end{pmatrix}${{</raw>}}  
  `\begin{pmatrix}1&2&3\\4&5&6\\7&8&9\end{pmatrix}`

* {{<raw>}}$\begin{vmatrix}1&2&3\\4&5&6\\7&8&9\end{vmatrix}${{</raw>}}  
  `\begin{vmatrix}1&2&3\\4&5&6\\7&8&9\end{vmatrix}`

* {{<raw>}}$\det\begin{pmatrix}1&2&3\\4&5&6\\7&8&9\end{pmatrix}${{</raw>}}  
  `\det\begin{pmatrix}1&2&3\\4&5&6\\7&8&9\end{pmatrix}`

### 積分  
* $\int (ax^2+bx+c)\ dx$  
  `\int (ax^2+bx+c)\ dx`  

* $\displaystyle\int (ax^2+bx+c)\ dx$  
  `\displaystyle\int (ax^2+bx+c)\ dx`  

* $\int^1_0 (ax^2+bx+c)\ dx$  
  `\int (ax^2+bx+c)\ dx`  

* $\displaystyle\int^1_0 (ax^2+bx+c)\ dx$  
  `\displaystyle\int^1_0 (ax^2+bx+c)\ dx`  

* {{<raw>}}$\iint_D xy\ dxdy\quad \{(x,y)|2< y<4,\ 2< x< 5\}${{</raw>}}  
  `\iint_D xy\ dxdy\quad \{(x,y)|2< y<4,\ 2< x< 5\}`  

* {{<raw>}}$\displaystyle \iint_D xy\ dxdy\quad \{(x,y) | 2< y< 4,\ 2< x< 5\}${{</raw>}}  
  `\displaystyle \iint_D xy\ dxdy\quad \{(x,y) | 2< y< 4,\ 2< x< 5\}`  

### 三角関数  
* $\sin(\theta)$  
  `\sin(\theta)`

* $\cos(\theta)$  
  `\cos(\theta)`

* $\tan(\theta)$  
  `\tan(\theta)`

* $\sin^{-1}(\theta)$  
  `\sin^{-1}(\theta)`

* $\cos^{-1}(\theta)$  
  `\cos^{-1}(\theta)`

* $\tan^{-1}(\theta)$  
  `\tan^{-1}(\theta)`

* $\arcsin(\theta)$  
  `\sin(\theta)`

* $\arccos(\theta)$  
  `\arccos(\theta)`

* $\arctan(\theta)$  
  `\arctan(\theta)`  

### 論理演算  
* $\lor$  
  `\lor`

* $\land$  
  `\land`

* $\lnot$  
  `\lnot`

* $\oplus$  
  `\oplus`  

* $\bar{a}$  
  `\bar{a}`  

* $\overline{ab}$  
  `\overline{ab}`

* $\forall$  
  `\forall`  

* $\exists$  
  `\exists`  


### 集合  
* {{<raw>}}$\{a_1,a_2,a_3\}${{</raw>}}  
  `\{a_1,a_2,a_3\}`. 

* $\in$  
  `\in`

* $\ni$  
  `\ni`

* $\notin$  
  `\notin`

* $\notni$  
  `\notni`  

* $\mid$  
  `\mid`  
  集合の内包的表記につかうといい感じに空白を調整してくれるらしい。

* $|$  
  `|`  

* $\cup$  
  `\cup`

* $\cap$  
  `\cap`  

* $\subset$  
  `\subset`

* $\subseteq$  
  `\subseteq`


* $\left(\dfrac{1}{2}\middle|\right)$  
  `\left`,`\right`の中でしか使えない。  
  これは空白をうまく調整してくれません。
  `\middle|`
  {{<raw>}}$\left\{\dfrac{1}{x}\middle|x\in\mathbb{Z}, x\bmod 2=0\right\}${{</raw>}}

* `\relmiddle`  
  これはデフォルトでは未定義なコマンドです。  
  次のように定義します。  
  `\gdef\relmiddle#1{\mathrel{}\middle#1\mathrel{}}`  
  $\gdef\relmiddle#1{\mathrel{}\middle#1\mathrel{}}$  
  {{<raw>}}$\displaystyle G = \left\{ (x,y) \in \mathbb{N}^2 \relmiddle| Z - \frac{1}{2} \leq x^2 + y^2 \leq Z + \frac{1}{2} \right\}${{</raw>}}  
  `\displaystyle G = \left\{ (x,y) \in \mathbb{N}^2 \relmiddle| Z - \frac{1}{2} \leq x^2 + y^2 \leq Z + \frac{1}{2} \right\}`
  > 引用元 https://zrbabbler.hatenablog.com/entry/20120411/1334151482

### ディスプレイスタイル(極限・総和)  
私の場合インラインスタイルの数式でディスプレイスタイルの$\lim$などを使いたいことがよくある。  
そういうときは`\displaystyle`を使う。  
* $\displaystyle \lim_{n\to\infty}$  
  `\displaystyle \lim_{n\to\infty}`
* $\displaystyle \sum_{n=0}^{\infty}$  
  `\displaystyle \sum_{n=0}^{\infty}`  

* {{<raw>}}$\displaystyle \sum_{\begin{subarray}{l}0< x< n\\ 0< y< n-1\end{subarray}}${{</raw>}}  
  `\displaystyle \sum_{\begin{subarray}{l}0< x< n\\ 0< y< n-1\end{subarray}}`  
  


# 応用編  
マクロ(コマンド)の使用  
マクロ(コマンド)を使うと$\LaTeX,\TeX$のコマンド(マクロ)を新たに作成できます。  
これは文章の中で特定の形が多く出てくる場合に便利です。  
例えば$\operatorname{Sin}$を考えよう。  
以前社会実践BFで使ったものだ。  
`\operatorname{Sin}`と毎回書いてもいいが文字を打つ量が多くなる。2回以上使う場合はマクロ(コマンド)を`\gdef\Sin{\operatorname{Sin}}`のようにするとこれを書いた場所より後では`\Sin`と書くだけで使えて便利だ。  
後になって変更することになっても`\gdef\Sin{\operatorname{Sin}}`の部分を変えるだけで対応できる。  

ベクトルを書くときにもこれを使うことができます。  
`\gdef\vect#1{\boldsymbol{#1}}`  
としておけば以後`\vect{a}`とするだけで使えるようになる上その後変えたいとなったときは`\gdef\vect#1{\boldsymbol{#1}}`の部分だけ変更すれば良いのです。



