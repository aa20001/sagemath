---
title: "Windows"
weight: 10
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

1. Download Installer  
  [sagemath.org](https://www.sagemath.org)を開き`Download`をクリックします。  
  * 方法1  
    ミラーサイトページに移動するので適当なミラーを選択します(近場を選ぶと早いことが多いです)。  
    `Microsoft Windows`をクリックします。  
  * 方法2  
    `For Windows installer binaries see the Windows GitHub release page`からgithubへいく  
  * 以後共通  
    必要なバージョンのインストーラー  
    `SageMath-{sage version}-Installer-v{installer version}.exe`  
    をクリックしダウンロードします。

1. Install SageMath  
    1. ダウンロードしたインストーラーをダブルクリックし起動します。  
      ただしコンピュータに存在する全ユーザー向けにインストールする場合は右クリックし`管理者として実行`をクリックしてください。  
      起動時に以下のような画面が表示されることがあります。  
      その場合`詳細情報`をクリックして`実行`をクリックしてください  
      <img src="windows_defender.png" style="margin-right: 10px"/><img src="windows_defender_confirm.png" />  
      管理者として実行した場合はUACダイアログが出ますので`はい`をクリックしてください  
      <img style="margin-right: 10px" src="runas_administrator.png" /><img src="UAC_dialog.png" />  
    1. インストールを進めます  
      `Next`をクリックします  
      <img src="step1.png" />  
    1. ライセンス契約に同意します  
      `I accept the agreement`の横にあるラジオボタンをクリックし`Next`をクリックします  
      <img src="step2.png" />  
    1. インストール対象を選択します。  
      全ユーザー向けにインストールしない場合はそのまま`Next`をクリックしてください  
      全ユーザー向けにインストールする場合は`Install for all users`の横にあるラジオボタンをクリックし`Next`をクリックしてください  
      <img src="step3.png" />
    1. Sageの初期ディレクトリの設定  
      SageMathはここで設定したディレクトリから起動します。  
      デフォルトのままにしておくのを推奨します(変更する場合ファイルのやりとりが少し面倒になります)  
      <img src="step4.png" />  
    1. インストール先の設定  
      `Next`をクリック  
      基本的にデフォルトのままで問題ありません。  
      <img src="step5.png" />  
    1. インストール内容の設定  
      `Next`をクリック  
      基本的にデフォルトのままで問題ありません。  
      HTML Documentationが不要な場合はチェックを外してください。  
      <img src="step6.png" />  
    1. ショートカットの設定  
      `Next`をクリック  
      デスクトップにショートカットが不要な場合は`Create desktop icons`の横のチェックボックスをクリックし、チェックを外す。  
      `Create start menu icons`のチェックを外すと起動がめんどくさくなるのでチェックを外さないこと。  
      <img src="step7.png" />  
    1. インストール内容の確認  
      内容を確認し`Install`をクリック  
      <img src="step8.png" />  
    1. インストール完了  
      `Finish`をクリック  
      <img src="step9.png" />  
      
      