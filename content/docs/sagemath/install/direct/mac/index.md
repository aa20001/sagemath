---
title: "Mac"
weight: 20
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

1. Doanload binary  
  [https://github.com/3-manifolds/Sage_macOS/releases]()より`.dmg`ファイルをダウンロードする。  
1. Install  
  ダウンロードした`.dmg`ファイルをダブルクリックしマウントする。  
  `SageMath-{version}.app`を`Applications`にドラッグ&ドロップする。  
1. Install(Optional)  
  シェルから`sage`を起動するまたはJupyterカーネルを使う場合は`Recommended-{version}.pkg`をダブルクリックしてインストールする。  