---
title: "Linux"
weight: 30
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

linuxにインストールする際はパッケージマネージャーを使ってインストールするのが楽です。  
リポジトリとの同期と必要に応じてパッケージの更新をしてから実行すると良いでしょう。  

* Ubuntu <18.04  
  `sudo apt-add-repository ppa:aims/sagemath`  
  `sudo apt-get install sagemath`  
* Ubuntu >= 18.04  
  `sudo apt install sagemath`  

* Archlinux  
  `pacman -S sagemath`  

ディストリビューションによってパッケージマネージャーは違います。  
適当なパッケージマネージャーを使いインストールしてください。  


* リポジトリにない場合ソースからインストールすることが可能です。  
  [https://doc.sagemath.org/html/en/installation/source.html]()  


