---
title: "WSLにインストール"
weight: 20
# bookFlatSection: false
# bookToc: true
bookHidden: true
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

1. Enable Windows Subsystem for Linux  
    1. スタートメニューを開き設定を開く  
      <img src="startmenu.png" />  
    1. `アプリ`をクリックする  
      <img src="settings_page1.png" />  
    1. 下にスクロールし`プログラムと機能`をクリックする。  
      <img src="settings_freatures.png" />. 
    