---
title: "Sagemath"
weight: 10
# bookFlatSection: false
# bookToc: true
# bookHidden: false
bookCollapseSection: true
# bookComments: false
# bookSearchExclude: false
---

Sagemathのインストール方法、基本的な使い方を書きます。  
ただしPythonに関連する部分(pip等)は別のページに書きます。
