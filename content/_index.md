---
title: "はじめに"
weight: 10
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

# はじめに  
このサイトではSageMathやそれに関連するJupyter Notebook,Jupyter Lab,Latexなどの話題を書いていこうと思います。  
このサイトは現在作成中です。完了するまで[旧サイト](https://aa20001.gitlab.io/sagemath_old/)を参照してください。

ここに書いてある情報に従って発生した損害等に関して私は責任を負いません。  
間違った情報や古い情報を発見した場合は[私](mailto:aa20001@cc.it-hiroshima.ac.jp)まで連絡していただくか[gitlab上](https://gitlab.com/aa20001/sagemath)で問題報告をしていただけると幸いです。

このサイト内で`{variable}`のようなものは`{variable}`を`variable`の内容で置き換える表記として書いてある部分があります。
